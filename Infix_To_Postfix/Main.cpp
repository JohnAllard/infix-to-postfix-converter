#include <iostream> 
#include <string> 
  
#include "InfixToPostfix.h" 
  
  
// -------@Author John Allard --------- 
// ------- CSIS 212 Laboratory #1 -------------- 
  
using namespace std; 
  
int main() 
{ 
    string input = ""; 
  
    cout << "Enter an expression: "; 
    cin >> input; 
    string output; 
	try
	{
		output = InfixToPostfix(input);
		cout << "\n" << output << "\n\n\n";
	}
    catch(logic_error e) 
	{
		cout << e.what();
	}
 
    system("pause"); 
  
  
}