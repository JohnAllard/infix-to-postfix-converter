#include <iostream> 
#include <string> 
#include "../../CS212/LinkedStack/LinkedStack.h" 
  
using namespace std; 
  
// ------- @Author John Allard ---------- 
// CSIS 212 Laboratory #1 
  
  
  
 bool isOperand(char); 
 bool isOperator(char); 
 int operatorComparison(char, char); 
  
// determines if the input is an operand by making sure it is in the set [a....z] 
bool isOperand(char input)  
{ 
    return(input >= 97 && input <= 122); 
} 
  
// Returns true if the input is a valid operator 
bool isOperator(char input)  
{ 
    return(input == '/' || input == '*' || input == '-' || input == '+'); 
} 
  
  
// This function compared the two operators for precedence, returns 1 if first has precedence,  
//  -1 if second has precedence, and 0 if they have equal precedence. 
int operatorComparison(char first, char second) { 
  
    if ((first == '*' || first == '/') && (second == '+' || second == '-'))  
        return 1; 
    else if ((first == '+' || first == '-') && (second == '*' || second == '/'))  
        return -1; 
    else
        return 0; 
  
} 
  
string InfixToPostfix(string input) 
{ 
    string postFixString = ""; 
    LinkedStack<char> opStack;            // this stack holds the operators 
    LinkedStack<char> parenthasisStack; // this stack makes sure the parenthasis match up and are valid. 
    bool waslastoperand = false; 
  
    // Loop through the array (one character at a time) until we reach the end of the string. 
    for(int i = 0; i < input.length(); i++) 
    { 
  
        //if it an operand we put it onto our final postfix string 
        if (isOperand(input[i]))  
        { 
            if(waslastoperand) 
                throw logic_error(" Invalid Expression - two operands next to eachother"); 
            else
            { 
                postFixString += input[i];  
                waslastoperand = true; 
            } 
        } 
        // If it is an operator, pop operators off our stack until it is empty, an open parenthesis or an operator with less than or equal precedence. 
        else if (isOperator(input[i]))  
        { 
            waslastoperand = false; 
            while (!opStack.isEmpty() && opStack.peek() != '(' && operatorComparison(opStack.peek(),input[i]) >= 0) 
            { 
                postFixString += opStack.peek(); 
                opStack.pop(); 
            } 
            opStack.push(input[i]); 
        } 
  
        // Simply push all open parenthesis onto our stack 
        // When we reach a closing one, start popping off operators until we run into the opening parenthesis. 
        else if (input[i] == '(')  
            {    
                waslastoperand = false; 
                parenthasisStack.push('('); // remember, this stack is only making sure the parenthasis are valid 
                opStack.push(input[i]); 
            } 
  
        else if (input[i] == ')')  
        { 
            waslastoperand = false; 
            // this part pops the last opening parenthasis off the parenthasis stack when it finds a closing parenthasis 
            if(!parenthasisStack.isEmpty()) 
                parenthasisStack.pop(); 
            // if that part failed it means we have an extra closing parenthasis 
            else
                throw logic_error("Invalid expression - Extra closing parenthasis"); 
  
            while (!opStack.isEmpty())  
            { 
                if (opStack.peek() == '(')  
                    opStack.pop();  
                else
                { 
                    postFixString += opStack.peek(); 
                    opStack.pop(); 
                } 
            } 
        } 
     
    }// end main while loop 
  
        // if this is true it means there was an extra opening parenthasis 
        if(!parenthasisStack.isEmpty()) 
            throw logic_error("Invalid expression - unmatched opening parenthasis"); 
  
        // if all else passed than we have a valid expression so we return the postfix string 
        return postFixString; 
}